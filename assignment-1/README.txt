# SYSC3303 Assignment 1

10/07/2018
Created by Igor Veselinovic 101011081 and Thao-Tran Le-Phuong 100997443.

## Files

Client.java - The source code for the Client class.
Proxy.java  - The source code for the Proxy class.
Server.java - The source code fot the Server class.

client_collaboration.png    - UML collaboration diagram for the client.
proxy_collaboration.png     - UML collaboration diagram for the proxy.
server_collaboration.png    - UML collaboration diagram for the servers.
UCM.pdf                     - UCM diagram showing client, proxy, and servers.
system_class_diagram.png	- UML class diagram for the system.

## Set-Up Instructions

1. Compile files in command line using the command `javac *.java`. If there are issues with this step, changing the path of the javac may help.
2. Start the servers by opening a separate terminal for each and running `java Server 1` to start Server1 and `java Server 2` to start Server2.
3. Open another terminal and run `java Proxy` to start the proxy.
4. Open another terminal and run `java Client` to start the client.
