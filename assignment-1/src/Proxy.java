/**
 * Proxy.java
 * This class is the proxy for assignment 1 for SYSC 3303. The proxy receives a
 * request from a client and sends it on to one of two clients randomly, then
 * sends the server's response back to the client.
 */

import java.io.*;
import java.net.*;
import java.util.Random;

public class Proxy {
	private DatagramSocket clientSendSocket, clientReceiveSocket;
	private DatagramSocket serverSendReceiveSocket;

	private DatagramPacket clientSendPacket, clientReceivePacket;
	private DatagramPacket serverSendPacket, serverReceivePacket;

	private InetAddress server1Address, server2Address;
	private int proxyPort, server1Port, server2Port;
	private final int packetSize;

	/**
	 * Constructor for the proxy.
	 *
	 * @param server1Address ip address of the first server
	 * @param server2Address ip address of the second server
	 * @param proxyPort      port that the proxy will listen on
	 * @param server1Port    port of the first server
	 * @param server2Port    port of hte second server
	 * @param packetSize     size of the packets to be sent and received
	 */
	public Proxy(InetAddress server1Address, InetAddress server2Address,
	             int proxyPort, int server1Port, int server2Port,
	             int packetSize) {
		this.server1Address = server1Address;
		this.server2Address = server2Address;
		this.proxyPort = proxyPort;
		this.server1Port = server1Port;
		this.server2Port = server2Port;
		this.packetSize = packetSize;

		try {
			/* Create the socket that sends packets to the client. */
			clientSendSocket = new DatagramSocket();

			/* Create the socket that receives packets from the client. */
			clientReceiveSocket = new DatagramSocket(proxyPort);

			/* Create the socket that sends and receives packets to/from the two
			 * servers. */
			serverSendReceiveSocket = new DatagramSocket();
		} catch (SocketException se) {
			se.printStackTrace();
			System.exit(1);
		}
	}

	/**
	 * Wait for a packet from a client, then randomly select one of two servers
	 * to send the packet to. Wait for a response from the selected server, then
	 * send the response packet from the server back to the original client.
	 */
	public void work() {
		/* Create the packet that will receive data from the client. */
		byte clientRequest[] = new byte[packetSize];
		clientReceivePacket = new DatagramPacket(clientRequest, packetSize);

		System.out.println("PROXY: Waiting for Packet...");

		/* Wait for a packet on the specified port. */
		try {
			clientReceiveSocket.receive(clientReceivePacket);
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(1);
		}

		System.out.println("PROXY: Packet Received.");
		Util.printReceivePacketInfo(clientReceivePacket);
		System.out.println();

		/* Get the client's address and port. These will be used to send the
		 * final response back to the client. */
		InetAddress clientAddress = clientReceivePacket.getAddress();
		int clientPort = clientReceivePacket.getPort();

		/* Randomly select one of the two servers to send the client's packet
		 * to. */
		Random r = new Random();
		InetAddress serverAddress;
		int serverPort;
		int randInt = r.nextInt(2);
		if (randInt == 0) {
			serverAddress = server1Address;
			serverPort = server1Port;
		} else {
			serverAddress = server2Address;
			serverPort = server2Port;
		}

		/* Create a packet that will go to the randomly selected server. */
		serverSendPacket = new DatagramPacket(clientRequest, packetSize,
		                                      serverAddress, serverPort);

		System.out.println("PROXY: Sending Packet...");
		Util.printSendPacketInfo(serverSendPacket);

		/* Send the packet to the randomly selected server. */
		try {
			serverSendReceiveSocket.send(serverSendPacket);
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(1);
		}

		System.out.println("PROXY: Packet Sent.\n");

		/* Create the packet that will receive data from the server. */
		byte serverResponse[] = new byte[packetSize];
		serverReceivePacket = new DatagramPacket(serverResponse, packetSize);

		System.out.println("PROXY: Waiting for Packet...");

		/* Wait for a packet from the server using the same socket and port as
		 * the one used for sending to the server. */
		try {
			serverSendReceiveSocket.receive(serverReceivePacket);
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(1);
		}

		System.out.println("PROXY: Packet Received.");
		Util.printReceivePacketInfo(serverReceivePacket);
		System.out.println();

		/* Create the packet that will be sent back to client. */
		clientSendPacket = new DatagramPacket(serverResponse, packetSize,
		                                      clientAddress, clientPort);

		System.out.println("PROXY: Sending Packet...");
		Util.printSendPacketInfo(clientSendPacket);

		/* Send the packet that came from the server back to the original
		 * client. */
		try {
			clientSendSocket.send(clientSendPacket);
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(1);
		}

		System.out.println("PROXY: Packet Sent.\n");
	}

	public static void main(String args[]) {
		try {
			/* Create the proxy, then wait for packages indefinitely. */
			InetAddress address = InetAddress.getLocalHost();
			Proxy p = new Proxy(address, address, 8080, 8081, 8082, 20);
			while (true) p.work();
		} catch (UnknownHostException e) {
			e.printStackTrace();
			System.exit(1);
		}
	}
}
