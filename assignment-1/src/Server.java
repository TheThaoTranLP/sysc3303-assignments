/**
 * Server.java
 * This class is the server side of assignment 1 for SYSC 3303. The server waits
 * for packet from a client/proxy, removes the vowels from the packet, then
 * sends it back.
 */

import java.io.*;
import java.net.*;
import java.util.regex.*;

public class Server {
	private DatagramPacket sendPacket, receivePacket;
	private DatagramSocket sendSocket, receiveSocket;
	private int packetSize;
	private String serverName;

	/**
	 * Constructor for the server.
	 *
	 * @param port       port that the server will listen on
	 * @param packetSize size of the packets to be received and sent
	 * @param serverName name of the server
	 */
	public Server(int port, int packetSize, String serverName) {
		try {
			/* Create the socket that sends packets to the client/proxy. */
			sendSocket = new DatagramSocket();

			/* Create the socket that receives packets from the client/proxy. */
			receiveSocket = new DatagramSocket(port);
		} catch (SocketException se) {
			se.printStackTrace();
			System.exit(1);
		}

		this.packetSize = packetSize;
		this.serverName = serverName;

	}

	/**
	 * Wait for a packet from the client/proxy, then remove the vowels and send
	 * a packet back with the vowels removed from the data received.
	 */
	public void work() {
		/* Create packet for receiving 20 character strings. */
		byte data[] = new byte[packetSize];
		receivePacket = new DatagramPacket(data, packetSize);

		System.out.println(serverName + ": Waiting for Packet...");

		/* Wait for a packet on the specified port. */
		try {
			receiveSocket.receive(receivePacket);
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(1);
		}

		System.out.println(serverName + ": Packet Received.");
		Util.printReceivePacketInfo(receivePacket);
		System.out.println();

		/* Create string from byte array in received packet. */
		String received = new String(data, 0, receivePacket.getLength());
		System.out.println(received + "\n");

		/* Remove vowels from the received string. */
		String send = received.replaceAll("[aeiouAEIOU]", "");

		/* Create a packet that will send the received string with the vowels
		 * removed back to client/proxy. */
		sendPacket = new DatagramPacket(send.getBytes(), send.length(),
		                                receivePacket.getAddress(),
		                                receivePacket.getPort());

		Util.printSendPacketInfo(sendPacket);
		System.out.println();

		/* Send the packet with the updated data back to the client/proxy. */
		try {
			sendSocket.send(sendPacket);
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(1);
		}

		System.out.println(serverName + ": Packet sent.");
	}

	public static void main( String args[] ) {
		/* Make sure a server number is specified on the command line. */
		if (args.length != 1) {
			System.out.println("Too few arguments (usage: java Server [server_num: 1 or 2])");
			System.exit(1);
		}

		int port = 8080;
		int serverNum = 0;

		try {
			serverNum = Integer.parseInt(args[0]);
		} catch (NumberFormatException e) {
			System.out.println("Server number can only be 1 or 2 (usage: java Server [server_num: 1 or 2])");
			System.exit(1);
		}

		if (serverNum != 1 && serverNum != 2) {
			System.out.println("Server number can only be 1 or 2 (usage: java Server [server_num: 1 or 2])");
			System.exit(1);
		}

		port += serverNum;

		/* Create the server, then wait for packages indefinitely. */
		Server s = new Server(port, 20, "SERVER" + args[0]);
		while (true) s.work();
	}
}
