/**
 * Client.java
 * This class is the client side of assignment 1 for SYSC 3303. The client sends
 * a random string to a server/proxy and awaits a response. This process is
 * repeated 100 times.
 */

import java.io.*;
import java.net.*;
import java.util.Random;
import java.util.Date;

public class Client {
	private DatagramSocket sendReceiveSocket;
	private DatagramPacket sendPacket, receivePacket;

	private InetAddress serverAddress;
	private int serverPort;
	private final int packetSize;

	/**
	 * Constructor for the client.
	 *
	 * @param serverAddress ip address of the server to send packets to
	 * @param serverPort    port on the server to send packets to
	 * @param packetSize    size of the packets to be sent and received
	 */
	public Client(InetAddress serverAddress, int serverPort, int packetSize) {
		this.serverAddress = serverAddress;
		this.serverPort = serverPort;
		this.packetSize = packetSize;

		/* Create the socket for sending packets to and receiving packets from
		 * the server. */
		try {
			sendReceiveSocket = new DatagramSocket();
		} catch (SocketException se) {
			se.printStackTrace();
			System.exit(1);
		}
	}

	/**
	 * Send a packet to a server and wait for the response. Print details about
	 * the sent and received packet.
	 */
	public void work() {
		/* Create the random string to be sent. */
		String str = getRandomString(packetSize);
		byte sendData[] = str.getBytes();

		/* Create the packet and populate it with the random string. Specify the
		 * address and port of the server as well. */
		sendPacket = new DatagramPacket(sendData, packetSize,
		                                serverAddress, serverPort);

		/* Print the packet info with the original unaltered random string. */
		System.out.println("CLIENT: Sending Packet...");
		Util.printSendPacketInfo(sendPacket);

		/* Send the packet to the server. */
		try {
			sendReceiveSocket.send(sendPacket);
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(1);
		}

		System.out.println("CLIENT: Packet Sent.\n");

		/* Create the packet that will receive data from the server. */
		byte receiveData[] = new byte[packetSize];
		receivePacket = new DatagramPacket(receiveData, packetSize);

		System.out.println("CLIENT: Waiting for Packet...");

		/* Wait for a packet from the server. */
		try {
			sendReceiveSocket.receive(receivePacket);
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(1);
		}

		/* Print the packet info, including the random string with vowels
		 * removed. */
		System.out.println("CLIENT: Packet Received.");
		Util.printReceivePacketInfo(receivePacket);
		System.out.println();
	}

	/**
	 * Generate a random string of a specified length.
	 *
	 * @param length number of characters in the string to be generated
	 * @return       a random string with the specified number of characters
	 */
	private String getRandomString(int length) {
		Random r = new Random();
		int rand;
		char str[] = new char[length];

		/* Build a string with the specified number of characters. Characters
		 * can be a-z or A-Z. */
		for (int i = 0; i < length; i++) {
			rand = r.nextInt(52);
			str[i] = rand < 26 ? (char) (rand + 'a') : (char) (rand - 26 + 'A');
		}

		return new String(str);
	}

	public static void main(String args[]) {
		try {
			/* Create the client, then send and receive 100 packages. */
			long startTime = (new Date()).getTime();
			Client client = new Client(InetAddress.getLocalHost(), 8080, 20);
			for (int i = 0; i < 100; i++) client.work();
			System.out.println("100 sends and 100 receives took " +
			                   ((new Date().getTime() - startTime)) + " ms.");
		} catch (UnknownHostException e) {
			e.printStackTrace();
			System.exit(1);
		}
	}
}
