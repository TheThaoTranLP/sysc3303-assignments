/**
 * Util.java
 * Helper methods used by classes for assignment 1 for SYSC 3303. Methods
 * include those for printing information about packets.
 */

import java.net.*;

public class Util {
	/**
	 * Print information about a packet that was/will be sent.
	 *
	 * @param packet the information of this packet is printed
	 */
	public static void printSendPacketInfo(DatagramPacket packet) {
		System.out.println("To host: " + packet.getAddress());
		System.out.println("Destination host port: " + packet.getPort());
		int len = packet.getLength();
		System.out.println("Length of data: " + len);
		System.out.println("Data: " + new String(packet.getData(), 0, len));
	}

	/**
	 * Print information about a packet that was received.
	 *
	 * @param packet the information of this packet is printed
	 */
	public static void printReceivePacketInfo(DatagramPacket packet) {
		System.out.println("From host: " + packet.getAddress());
		System.out.println("Host port: " + packet.getPort());
		int len = packet.getLength();
		System.out.println("Length of data: " + len);
		System.out.println("Data: " + new String(packet.getData(), 0, len));
	}
}
