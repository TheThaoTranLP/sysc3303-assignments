# SYSC3303 Assignment 2

Date: 20/07/2018  
Created by Igor Veselinovic 101011081 and Thao-Tran Le-Phuong 100997443.

## Run Instructions
1. Run `javac *.java` in the src folder to compile the class files.
2. Run `java Vacationer` to run the program.
3. Enter 'q' at any time while the program is running to quit the program. The program will continue to run until all the threads have completed their fishing cycle and then quit. If 'q' is never entered, the program will run indefiinitely.
