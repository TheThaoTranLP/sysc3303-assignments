/**
 * This class defines the behaviour of a vacationer who can fish and observe
 * their surroundings, dependent upon the availablity of fishing rods and nets.
 */

import java.util.Random;

public class Vacationer implements Runnable {
	private SynchronizedSet<FishingRod> rods;
	private SynchronizedSet<Net> nets;
	private String name;

	private FishingRod heldRod;
	private Net heldNet;

	private Random rand;
	private boolean continueFishing;

	/**
	 * Constructor for the Vacationer class.
	 *
	 * @param rods  set of fishing rods for the vacationer to take from
	 * @param nets  set of nets for the vacationer to take from
	 * @param name  identifier for the vacationer
	 */
	public Vacationer(SynchronizedSet<FishingRod> rods,
	                  SynchronizedSet<Net> nets, String name) {
		this.rods = rods;
		this.nets = nets;
		this.name = name;

		rand = new Random();
		this.continueFishing = true;
	}

	/**
	 * Vacationer gets a rod and a net and fishes for a few seconds before
	 * putting everything down and observes the scenery.
	 */
	public void run() {
		while (continueFishing) {
			System.out.println(name + " is picking up a rod");
			heldRod = rods.take();
			System.out.println(name + " has picked up " + heldRod.getName());
			try {
				Thread.sleep(1000);
				System.out.println(name + " is picking up a net");
				heldNet = nets.take();
				System.out.println(name + " has picked up " + heldNet.getName());
				System.out.println(name + " is fishing");
				Thread.sleep(rand.nextInt(2000) + 1000);
				System.out.println(name + " is putting down " + heldRod.getName());
				rods.put(heldRod);
				System.out.println(name + " has put down " + heldRod.getName());
				Thread.sleep(1000);
				System.out.println(name + " is putting down " + heldNet.getName());
				nets.put(heldNet);
				System.out.println(name + " has put down " + heldNet.getName());
				System.out.println(name + " is observing the scenery");
				Thread.sleep(rand.nextInt(2000) + 2000);
			} catch (InterruptedException e) {
				e.printStackTrace();
				System.exit(1);
			}
		}

		System.out.println(name + " is done fishing");
	}

	public void setContinueFishing() {
		this.continueFishing = false;
	}

	public static void main (String args[]) {
		/* Initialize the sets of fishing rods and nets. */
		SynchronizedSet<FishingRod> rods = new SynchronizedSet<FishingRod>(5);
		SynchronizedSet<Net> nets = new SynchronizedSet<Net>(5);
		Vacationer[] vacationers = new Vacationer[10];
		for (int i = 1; i <= 5; i++) {
			rods.put(new FishingRod("Rod_" + i));
			nets.put(new Net("Net_" + i));
		}

		/* Create and start the Vacationer threads. */
		for (int i = 1; i <= 10; i++) {
			vacationers[i - 1] = new Vacationer(rods, nets, "Vacationer_" + i);
			Thread t = new Thread(vacationers[i - 1]);
			t.start();
		}

		/* Create and start thread to listen for a quit command from user. */
		Thread t = new Thread(new QuitListener(vacationers));
		t.start();
	}
}
