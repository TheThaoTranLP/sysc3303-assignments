/**
 * This class provides a basic representaton for a fishing net.
 */

public class Net {
	private String name;

	/**
	 * Constructor for the net.
	 *
	 * @param name string used to identify the net
	 */
	public Net(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}
}
