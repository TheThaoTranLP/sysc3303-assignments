/**
 * This class defines a simple wrapper for a set that has fixed capacity and
 * thread-safe put and take operations.
 */

import java.util.HashSet;
import java.util.Iterator;

public class SynchronizedSet<E> {
	HashSet<E> set;
	int capacity;

	/**
	 * Constructor for the synchronized set with a fixed capacity.
	 *
	 * @param capacity maximum number of elements allowed in the set
	 */
	public SynchronizedSet(int capacity) {
		set = new HashSet<E>();
		this.capacity = capacity;
	}

	/**
	 * Adds an element to the set as long as the set isn't full. If the set is
	 * full, then wait until it no longer is to put something the set.
	 */
	public synchronized void put(E element) {
		/* Wait until the set isn't full */
		while (set.size() == capacity) {
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
				System.exit(1);
			}
		}

		set.add(element);
		notify();
	}

	/**
	 * Gets and removes an element from the set. If the set is empty, then wait
	 * until it no longer is to take something from the set.
	 */
	public synchronized E take() {
		/* Wait until the set isn't empty */
		while (set.isEmpty()) {
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
				System.exit(1);
			}
		}

		E taken = set.iterator().next();
		set.remove(taken);
		notify();

		return taken;
	}
}
