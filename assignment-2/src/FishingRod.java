/**
 * This class provides a basic representaton for a fishing rod.
 */

public class FishingRod {
	private String name;

	/**
	 * Constructor for the fishing rod.
	 *
	 * @param name string used to identify the fishing rod
	 */
	public FishingRod(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}
}
