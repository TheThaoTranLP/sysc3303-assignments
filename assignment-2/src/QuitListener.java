/**
 * Listens for a quit command from the user.
 */

import java.util.Scanner;

public class QuitListener implements Runnable {
	Vacationer[] vacationers;

	/**
	 * Constructor for QuitListener class.
	 *
	 * @param vacationers array of running Vacationer instances
	 */
	public QuitListener(Vacationer[] vacationers) {
		this.vacationers = vacationers;
	}

	/**
	 * Listeners for a quit command from the user.
	 */
	public void run() {
		Scanner s = new Scanner(System.in);

		while (!s.next().toLowerCase().equals("q") &&
		       !s.next().toLowerCase().equals("quit")) {}

		for (int i = 0; i < vacationers.length; i++) {
			vacationers[i].setContinueFishing();
		}
	}
}
